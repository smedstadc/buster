import * as React from 'react'

export default class Hello extends React.Component {
  render () {
    return (
      <h1 className="Hello">Hi, World</h1>
    )
  }
}