
desc "install master key on prod"
task "key" do
  on roles(:app) do |host|
    execute :mkdir, "-pv", "/var/www/rails/shared/config"
    Net::SCP.upload!(host.hostname, "root", "config/master.key", "/var/www/rails/shared/config/master.key")
  end
end
